#!/bin/bash

##################################################################
# MultiQC summarization of FastQC outputs for primer-trimmed reads
##################################################################

STEP_LOGS_DIR='logs/multiqc'
if [ ! -d ${STEP_LOGS_DIR} ]; then mkdir -p ${STEP_LOGS_DIR}; fi

# Reminder: .sbatch file must be specified LAST for options passed at command line to be considered

sbatch \
	--cpus-per-task 1 \
	--mem '512M' \
	--output ${STEP_LOGS_DIR}/MultiQC_%A_%a.txt \
	multiqc_primer_trimmed.sbatch
