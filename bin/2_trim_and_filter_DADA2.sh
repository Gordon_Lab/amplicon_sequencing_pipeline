#!/bin/bash

#############################################
# 2. Perform DADA2-appropriate read filtering
#############################################

# Description:
#    Using the dada2 R package, trim (to a specified length) the R1 and R2 sequences and
#    filter the truncated reads such that: (i) any read containing more than a specified 
#    number of expected errors is removed; and, (ii) any read containing one or more 'N' bases
#    is removed; only R1/R2 pairs for which both reads meet the filtering criteria will 
#    progress to the next pipeline step

# This script runs the corresponding .sbatch script, passing its options via
# command line (instead of within the .sbatch script itself).
# This allows me to:
#    -Use interpolation to pull SBATCH options from the config.sh file
#    -Use command substitution to define the number of tasks using the mapping file

# Retrieve definitions for variables not defined within this script
source "./config.sh"

# Count all lines of mapping file that aren't a header line (header line starts with "#")
JOB_COUNT=$( grep "^[^#]" ${MAPPING_FILE_PATH} | wc -l | awk '{print $1}' )

STEP_LOGS_DIR=${LOGS_DIR}/${FILTER_LOGS_BASENAME}
if [ ! -d ${STEP_LOGS_DIR} ]; then mkdir -p ${STEP_LOGS_DIR}; fi

# By default no number of maximum tasks is defined for this step (adjust manually if needed)

# Reminder: .sbatch file must be specified AFTER sbatch options for those options to be considered
sbatch \
        --array 1-${JOB_COUNT} \
        --cpus-per-task ${FILTER_CPU} \
	--mem ${FILTER_MEM} \
        --mail-type ${FILTER_NOTIFY} \
        --mail-user ${FILTER_EMAIL} \
        --output ${STEP_LOGS_DIR}/${FILTER_LOGS_BASENAME}_%A_%a.txt \
        2_trim_and_filter_DADA2.sbatch ${MAPPING_FILE_PATH} ${FILTER_TRUNCLEN_R1} ${FILTER_TRUNCLEN_R2} ${FILTER_MAXEE_R1} ${FILTER_MAXEE_R2}
