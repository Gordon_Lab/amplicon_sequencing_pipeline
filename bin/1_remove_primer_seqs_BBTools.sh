#!/bin/bash

#######################
# 1. Read preprocessing
#######################

# Description:
#   i)   Determine which R1 and R2 reads contain identifiable 515F and 806R primer sequences,
#        searching in both the PS1 and PS2 orientations
#   ii)  Trim primer sequences from those reads, along with any leading phasing bases
#   iii) Re-pair the trimmed R1 and R2 sequences; only R1/R2 pairs for which a 515F and
#        806R sequence were identified in the original data will progress to the
#        next pipeline step

# This script runs the corresponding .sbatch script, passing its options via
# command line (instead of within the .sbatch script itself).
# This allows me to:
#    -Use interpolation to pull SBATCH options from the config.sh file
#    -Use command substitution to define the number of tasks using the mapping file

# Retrieve definitions for variables not defined within this script
source "./config.sh"

# Count all lines of mapping file that aren't a header line (header line starts with "#")
JOB_COUNT=$( grep "^[^#]" ${MAPPING_FILE_PATH} | wc -l | awk '{print $1}' )

STEP_LOGS_DIR=${LOGS_DIR}/${PREPROCESSING_LOGS_BASENAME}
if [ ! -d ${STEP_LOGS_DIR} ]; then mkdir -p ${STEP_LOGS_DIR}; fi

# By default no number of maximum tasks is defined for this step (adjust manually if needed)

# Reminder: .sbatch file must be specified AFTER sbatch options for those options to be considered
sbatch \
        --array 1-${JOB_COUNT} \
        --cpus-per-task ${PREPROCESSING_CPU} \
        --mem ${PREPROCESSING_MEM} \
        --mail-type ${PREPROCESSING_NOTIFY} \
        --mail-user ${PREPROCESSING_EMAIL} \
        --output ${STEP_LOGS_DIR}/${PREPROCESSING_LOGS_BASENAME}_%A_%a.txt \
        1_remove_primer_seqs_BBTools.sbatch ${MAPPING_FILE_PATH}
