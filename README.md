# Gordon Lab Amplicon Sequencing Workflow (v1.31)

## Acknowledgements and workflow history
In 2018, Matt Hibberd (MCH) designed and wrote the original Gordon Lab amplicon sequencing workflow (v1.1). That version of the workflow served as the lab's default until 2022 when the high-throughput computing facility (HTCF) underwent a significant reconfiguration which rendered v1.1 inoperable.

In 2023, Nate McNulty (NPM) introduced updates that: (i) enabled the workflow to run on the latest iteration of the HTCF, and (ii) streamlined its mechanics. Minor technical improvements were also introduced, but the principal algorithmic logic of MCH's original workflow was left intact.

NPM authored the v1.31 documentation that follows. Any errors are his own.

## Overview

This workflow was created to enable members of the Gordon Lab to quickly and easily perform amplicon sequence variant (ASV) calling on the HTCF. It subjects paired-end 16S rRNA gene amplicon sequence data from the V4 hypervariable region to a series of necessary preprocessing steps, after which the DADA2 denoising algorithm is used to identify ASVs. The final result is an ASV frequency table for all samples/data considered. This ASV table is a suitable starting point for any number of downstream analyses the user might wish to pursue. Those downstream analyses will typically be performed within R on a user's local system.

## Inputs

- Previously demultiplexed paired-end V4 amplicon data (R1/R2 files)
- A tab-delimited mapping file (column 1: SampleID, column 2: MatchID; additional columns permitted but ignored)
- A configuration file that defines all settings/parameters for your analysis

## General architecture
- The authoritative reference for this workflow is maintained as project `amplicon_sequencing_pipeline` on the Gordon Lab GitLab account. It is currently being maintained by MCH and NPM.
- To run this workflow on HTCF, users must load a Spack package built from a particular tagged version of this repo.
- The workflow is run on HTCF as a series of 3 shell scripts. Each shell script triggers the submission of an sbatch job tailored to the particulars of the analysis using settings provided in the configuration file.
- Typically, a user can expect to have an ASV table generated from their data within a few hours or less when running this workflow.

## Summary protocol

1. Organize your inputs
2. Filter your starting set of reads down to the subset of R1/R2 pairs that contain identifiable 515F/806R sequences, then trim off those primer sequences
3. Length-trim and quality-filter each surviving R1/R2 pair
4. Merge surviving R1/R2 pairs and perform ASV calling
5. QC the outputs
6. Clean up any files that are no longer needed, and finalize the documentation for your analysis

## Detailed protocol
1. Load the Spack package for the workflow to place all of the needed `.sh`/`.sbatch`/`.R` files within your PATH:

    `eval $(spack load --sh amplicon-sequencing-pipeline)`
2. Create a project folder (on scratch) and navigate into it:

    `mkdir YYYYMMDD_StudyName_V4-16S`
    
    `cd YYYYMMDD_StudyName_V4-16S`
3. Create a folder within the project folder to which your demultiplexed raw data can be copied:

    `mkdir demux`
4. Use `rsync` to copy the demultiplexed R1 and R2 files (previously archived on LTS) to the new folder:

    `rsync -vah <source> demux`

    _Note: Your R1 and R2 file names must end with the strings `_R1_001.fastq.gz` and `_R2_001.fastq.gz`, respectively. This is typically the case for files provided by DSIL._
    
    _Tip: Consider storing your rsync command in a `.sh` file and then running that shell script to perform the transfer. This will document where your starting data came from and make it easier to reconstitute your input data should you need it in the future (the original data will be deleted at the end of the workflow to conserve disk space)._
    
5. Retrieve example files:

    `get_example_files.sh`
    
    _These will include `mapping_file_example.txt` (see step #6), `config_example.sh` (see step #7), and a number of example code files that can be used as a starting point for downstream analyses after the ASV table is created. All of these files will be stored in new folder `example_files`._

6. Prepare a tab-delimited mapping file with the same formatting as `example_files/mapping_file_example.txt`. Place it in your top level project folder.

    _Column #1_: The sample identifier for the sequenced library (SampleID)
    
    _Column #2_: The unique leading string used to name the R1/R2 files for a library (MatchID)

    _Column #3 (optional)_: The index sequence assigned to the sequenced library (Index)

    The mapping file allows each sequencing library, described by a sample identifier (SampleID), to be matched up with the pair of demultiplexed sequencing results (R1 and R2 files) that library gave rise to. Because DSIL's naming conventions for demultiplexed sequencing results have evolved over the years, the workflow assumes only that each R1/R2 pair will be named using a unique string that precedes `_R1_001.fastq.gz` and `_R2_001.fastq.gz`. That unique string is included in the mapping file as the MatchID.

    Assume one of your results files is named as follows:

    `Gordon_CGGCAAATACACT_RC_AGTGTATTTGCCG_S341_R1_001.fastq.gz`
    
    This is one way you might create a mapping file for such results:
    
    - Collect your MatchID strings (using the R1 filenames as a starting point):

      `find . -name "*_R1_001.fastq.gz" -exec basename {} \; | sed "s/_R1_001.fastq.gz//" > match_ids.txt`

      Example output: `Gordon_CGGCAAATACACT_RC_AGTGTATTTGCCG_S341`
    - Open `match_ids.txt` in Excel
    - Duplicate the first column
    - Separate (newly created) column #2 using `_` as the delimiter; delete all newly created columns except the one containing the index sequence.
    - Use VLOOKUP() in conjunction with Marty's library prep records to populate column #3 with a SampleID corresponding to each index sequence
    - Reorder columns as: SampleID, MatchID, Index (Index is optional)
    - If desired, add column headers (line must start with a `#`)
    - Save the mapping file in tab-delimited text format within the top level of your project folder.
    
    _Note: If your sequencing data include results from standards (e.g., an ATCC mock community), decide at this point if you want to include or exclude those results in the ASV calling process._

7. Update `config_example.sh` with parameters suitable for your study. Once it's ready to go, rename it `config.sh` and store it in your top level project folder..

    - At a minimum, you'll need to update this line with the name of your mapping file from step #6 and add your contact email for job tracking at each main step:

    `export MAPPING_FILE_PATH=${WORK_DIR}/<mapping file name>`

 _\*\*\*Make sure you are positioned within the top level of your project folder when running steps 8-10.\*\*\*_

8. Filter the R1 and R2 files down to those pairs that contain both a 515F and an 806R primer sequence, then remove the primer sequences and any leading phasing bases.

    `1_remove_primer_seqs_BBTools.sh`

    - Primer matching outputs will be stored in the paths provided for `P515F_DIR`, `P806R_DIR` in your `config.sh` file. The paired reads carried forward to the next step will be stored in folder `paired`.

    Recommendation: Adjust your read truncation settings (`FILTER_TRUNCLEN_R1`/`FILTER_TRUNCLEN_R2` within `config.sh`) to be used in step 9 using the quality scores for your primer-trimmed reads (i.e., the outputs of this step) in one of two ways:

    - Manually inspect the sequence quality metrics for your primer-trimmed reads using FastQC and MultiQC
        - Scripts that make this step easier to perform on HTCF will eventually be incorporated into the default workflow; until then,
          example scripts can be found in the `dev/find_trunc_length` folder on GitLab. Please see NPM for guidance on how to run them.
    - Use a purpose-built tool such as Zymo's FIGARO (this approach has not yet been tested but looks promising)

9. Trim the surviving read pairs and filter by quality to generate inputs suitable for ASV calling.

    `2_trim_and_filter_DADA2.sh`

    - Outputs will be stored in the 'filtered' folder.

10. Merge each pair of R1 and R2 reads, perform ASV calling, and output an ASV table formatted as a serialized R object.

    `3_call_ASVs_DADA2.sh`
    
    - Outputs will be stored as: (i) a `.seqtab` file in folder `seqtabs` (the ASV table that can be carried forward for further analysis), and (ii) R object `dada2.Rdata` (a snapshot of the R workspace created in previous steps).  

11. [Eventually add recommendations for QC and file clean-up here.]

## Key dependencies utilized
- BBMap (v38.63)
- DADA2 (R, v1.26.0)

## References
[**DADA2: High-resolution sample inference from Illumina amplicon data.**](https://www.nature.com/articles/nmeth.3869) Callahan BJ, McMurdie PJ, Rosen MJ, Han AW, Johnson AJ, Holmes SP. Nature Methods (2016)

[**Exact sequence variants should replace operational taxonomic units in marker-gene data analysis.**](https://www.nature.com/articles/ismej2017119) Callahan BJ, McMurdie PJ, Holmes SP. ISME (2017)
